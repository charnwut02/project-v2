import { Component, OnInit } from '@angular/core';
import User from '../entity/user';
// import { UserService } from '../service/user.service';
import { Router } from '@angular/router';
import { StudentService } from '../service/student-service';
import Student from '../entity/student';
import Teacher from '../entity/teacher';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  student: Student[];
  email: string;
  password: string;
  model: Student = new Student();
  


  

  constructor(private studentService: StudentService, private router: Router) { }

  ngOnInit() {
    this.studentService.getStudents()
      .subscribe(student => this.student = student);
  }
  onSubmit() {    
    this.studentService.getStudentEmailAndPassword(this.model.email, this.model.password)
      .subscribe((student) => {
        if (student.id != null) {          
          this.router.navigate(['/home']);
        }
      }, (error) => {
        alert('could not save value');
      });
  }
 
 
  // login(): void {
  //   let isValid = false;
  //   if (Array.isArray(this.students)) {
  //     for (const student of this.students) {
  //       if (this.email === student.email && this.password === student.password) {
  //         isValid = true;
  //       }
  //     }

  //     if (isValid) {
  //       this.router.navigate(['home']);
  //     } else {
  //       alert("Invalid");
  //     }
  //   } else {
  //     return null;
  //   }
  // }

}

