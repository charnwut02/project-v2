import { Injectable } from '@angular/core';
import { TeacherService } from './teacher.service';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import Teacher from '../entity/teacher';

@Injectable({
  providedIn: 'root'
})
export class TeachersRestImplService extends TeacherService{
  
  getTeachers(): Observable<Teacher[]> {
    return of(this.teacher);
  }
 
  getTeacher(id: number): Observable<Teacher> {
    return this.http.get<Teacher[]>('assets/people.json').pipe(map(teachers => {
      const output: Teacher = (teachers as Teacher[]).find(teacher => teacher.id === +id);
      return output;
    }))
  }
    
  getTeacherEmailAndPassword(email: String, password: String): Observable<Teacher> {
    const output: Teacher = this.teacher.find(teacher => teacher.email === email && teacher.password === password);
    return of(this.teacher[output.id-1]);
  }


  constructor(private http: HttpClient) {
    super();
  }

  teacher:Teacher[] = [{
      "id": 1,
      "name":"Aj.A",
      "surname":"AA",
      "email":"A",
      "password":"1234"
  },{
      "id": 2,
      "name":"Aj.B",
      "surname":"BB",
      "email":"B",
      "password":"1234"
  },{
    "id": 3,
    "name":"Aj.C",
    "surname":"CC",
    "email":"C",
    "password":"1234"
}];
}