import { Injectable } from '@angular/core';
import { StudentService } from './student-service';
import { Observable, of } from 'rxjs';
import Student from '../entity/student';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StudentsRestImplService extends StudentService {

  constructor(private http: HttpClient) {
    super();
  }

  getStudents(): Observable<Student[]> {
    return of(this.student);
  }
  getStudentEmailAndPassword(email: String, password: String): Observable<Student> {
    const output: Student = this.student.find(student => student.email === email && student.password === password);
    return of(this.student[output.id - 1]);
  }

  getStudent(id: number): Observable<Student> {
    return this.http.get<Student>(environment.studentApi + '/' + id);
  }

  saveStudent(student: Student): Observable<Student> {
    student.id = this.student.length + 1;
    this.student.push(student);
    return this.http.post<Student>(environment.studentApi, student);
  }
  updateStudent(student: Student): Observable<Student> {
    let id = student.id;
    this.student[id - 1] = student;
    return of(student);
  }
  student: Student[] = [{
    id: 1,
    studentId: "592115009",
    name: "Charnwut",
    surname: "Thopurin",
    image: "",
    password: "nui",
    email: "charnwut@hotmail.com"

  }, {
    id: 2,
    studentId: "592115003",
    name: "Kittinut",
    surname: "Saengsri",
    image: "",
    password: "tem",
    email: "kittinut@hotmail.com"
  }, {
    id: 3,
    studentId: "592115024",
    name: "Phumsiri",
    surname: "Jiraponsawad",
    image: "",

    password: "5678",
    email: "5678@gmail.com"
  }];


}
