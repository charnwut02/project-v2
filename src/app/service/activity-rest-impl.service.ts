import { Injectable } from '@angular/core';
import { ActivityService } from './activity.service';
import { HttpClient } from '@angular/common/http';
import { StudentsRestImplService } from './students-rest-impl.service';
import Activity from '../entity/activity';
import { Observable } from 'rxjs/internal/Observable';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ActivityRestImplService extends ActivityService {
  saveEnrollActivity(id: number) {
    this.enroll.push(this.activity[id - 1]);
    console.log(this.enroll);
  }
  getEnrollActivity(): Observable<Activity[]> {
    return of(this.enroll);
  }
  check(id: number): boolean {
    if (this.enroll.find(activity => activity.id == id)) {
      return false;
    } else {
      return true;
    }
  }

  getActivitys(): Observable<Activity[]> {
    return of(this.activity);
  }
  getActivity(id: number): Observable<Activity> {
    return of(this.activity[id - 1]);
  }


  constructor(private http: HttpClient, private student: StudentsRestImplService) {
    super();
  }

  saveActivity(activity: Activity): Observable<Activity> {
    activity.id = this.activity.length + 1;
    this.activity.push(activity);
    return of(activity);
  }
  enroll: Activity[] = [];
  activity: Activity[] = [
    {
      "id": 1,
      "actId": "a-001",
      "actName": "pingpong",
      "date": "12/12/18",
      "location": "camt",
      "period": "12:00-13:00",
      "description": "fun activity",
      "actHost": "aj. nui"
    }, {
      "id": 2,
      "actId": "a-002",
      "actName": "boxing",
      "date": "12/12/18",
      "location": "camt",
      "period": "14:00-16:00",
      "description": "fun activity",
      "actHost": "aj. tem"
    }
  ]
}

