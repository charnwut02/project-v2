import { Component, OnInit } from '@angular/core';
import { StudentService } from '../service/student-service';
import { Router } from '@angular/router';
import Student from '../entity/student';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent {
  model: Student = new Student();

  constructor(private studentService: StudentService, private router: Router) { }

  onSubmit() {
    console.log(this.model);
    this.studentService.saveStudent(this.model)    
      .subscribe((student) => {
        this.router.navigate(['/login']);
      }, (error) => {
        alert('could not save value');
      });
  }


}
