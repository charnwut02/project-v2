import { Component, OnInit, ViewChild } from '@angular/core';
import { EnrollActivityDataSource } from './enroll-activity-datasource';
import { MatPaginator, MatSort } from '@angular/material';
import Activity from 'src/app/entity/activity';
import { BehaviorSubject } from 'rxjs';
import { ActivityService } from '../../service/activity.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-enroll-activity',
  templateUrl: './enroll-activity.component.html',
  styleUrls: ['./enroll-activity.component.css']
})
export class EnrollActivityComponent implements OnInit {

  model: Activity  = new Activity();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: EnrollActivityDataSource ;
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'actId', 'actName', 'location', 'date',  'actHost', 'period', 'description'];
  activity: Activity[];
  filter: string;
  filter$: BehaviorSubject<string>;
  constructor(private activityService: ActivityService, private router:Router) { }
  ngOnInit() {
    this.activityService.getActivitys()
      .subscribe(activitys => {
        this.dataSource = new EnrollActivityDataSource(this.paginator,this.sort);
        this.dataSource.data = activitys;
        this.activity = activitys;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
      }
      );
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
    this.filter$.next(filterValue.trim().toLowerCase());
  }
  enroll(activity: any) {
    if (this.activityService.check(activity.id)){
      this.activityService.saveEnrollActivity(activity.id);
      this.router.navigate(['/listEnroll']);
    }else{
      alert('Can not enroll');
    }
    
    
}
get diagnostic() { return JSON.stringify(this.model);}
}
