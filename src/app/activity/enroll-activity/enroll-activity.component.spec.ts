import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnrollActivityComponent } from './enroll-activity.component';

describe('EnrollActivityComponent', () => {
  let component: EnrollActivityComponent;
  let fixture: ComponentFixture<EnrollActivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnrollActivityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnrollActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
