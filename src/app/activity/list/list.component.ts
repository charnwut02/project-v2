import { Component, OnInit } from '@angular/core';
import { ActivityService } from '../../service/activity.service';
import { ActivityRestImplService } from '../../service/activity-rest-impl.service';
import Activity from '../../entity/activity';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  activitys : Activity;

  constructor(private activityService: ActivityRestImplService,private route:ActivatedRoute) { }

  ngOnInit() {
    this.route.params
    .subscribe((params: Params) => {
      this.activityService.getActivity(+params['id'])
        .subscribe((inputActivity: Activity) => this.activitys = inputActivity);
    });
  }

}
