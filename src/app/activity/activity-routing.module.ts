import { Routes, RouterModule } from '@angular/router';
import { AddComponent } from './add/add.component';
import { NgModule } from '@angular/core';

import { RegistrationComponent } from '../registration/registration.component';
import { LoginComponent } from '../login/login.component';
import { HomeComponent } from '../home/home.component';
import { ListComponent } from './list/list.component';
import { EditComponent } from './edit/edit.component';
import { ActivityTableComponent } from './activity-table/activity-table.component';
import { EnrollActivityComponent } from 'src/app/activity/enroll-activity/enroll-activity.component';



const ActivityRoutes: Routes = [
    { path :'addAct' ,component: AddComponent },
    { path :'tableAct' ,component: ActivityTableComponent},
    { path :'listEnroll',component: EnrollActivityComponent},
    { path : 'detailAct/:id',component: ListComponent }


];
@NgModule({
    imports: [
        RouterModule.forRoot(ActivityRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class ActivityRoutingModule {

}
