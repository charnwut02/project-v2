import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ActivityTableDataSource } from './activity-table-datasource';
import Activity from '../../entity/activity';
import { BehaviorSubject } from '../../../../node_modules/rxjs';
import { ActivityService } from '../../service/activity.service';
import { SelectionModel } from '../../../../node_modules/@angular/cdk/collections';
import { Router } from '@angular/router';

@Component({
  selector: 'app-activity-table',
  templateUrl: './activity-table.component.html',
  styleUrls: ['./activity-table.component.css']
})
export class ActivityTableComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource = new MatTableDataSource<Activity>();
  selection = new SelectionModel<Activity>(true, []);
  displayedColumns = ['id','actId', 'actName', 'location', 'description', 'period', 'date', 'actHost','enroll'];
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */

  activitys: Activity[];
  filterValue: string;
  filter: string;
  filter$: BehaviorSubject<string>;
  loading: boolean;

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  constructor(private activityService: ActivityService, private router: Router) { }
  ngOnInit() {
    this.loading = true;
    this.activityService.getActivitys()
      .subscribe(activitys => {
        this.dataSource = new MatTableDataSource<Activity>();
        this.dataSource.data = activitys;
        this.activitys = activitys;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.sort = this.sort;
      });
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  enroll(activity:any){
    if(this.activityService.check(activity.id)){
      this.activityService.saveEnrollActivity(activity.id);
      this.router.navigate(['/listEnroll']);
    }else{
      alert('could not enroll')
    }
  }
  }
