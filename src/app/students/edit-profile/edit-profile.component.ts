import { Component, OnInit } from '@angular/core';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { StudentService } from '../../service/student-service';
import Student from '../../entity/student';
import { FormGroup, FormBuilder, Validators, RequiredValidator } from '@angular/forms';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
  student: Student;
  model:Student;
  sf: FormGroup;
  fb: FormBuilder;
  constructor(private route: ActivatedRoute, private studentService: StudentService,private router:Router) { }
  ngOnInit() {
    this.route.params
    .subscribe((params: Params) => {
      this.studentService.getStudent(+params['id'])
        .subscribe((inputStudent: Student) => this.student = inputStudent);
    });
    this.sf = this.fb.group({
      studentId: [this.student.studentId,Validators.compose([Validators.required,
      Validators.maxLength(10),Validators.pattern('[0-9]+')])],
      name: [this.student.name ,Validators.required],
      surname: [this.student.surname ,Validators.required],
      imge: [this.student.image],
      email: [this.student.email, Validators.compose([Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')])],
      password:[this.student.password, Validators.required]
    });
  
  }
 
onSubmit(){
  this.model = this.sf.value
  this.model.id = this.student.id;
  this.studentService.updateStudent(this.model)
  .subscribe((student) => {
    this.router.navigate(['detail',student.id]);
  },(error)=>{
    alert('could not update');
  });
}

}
